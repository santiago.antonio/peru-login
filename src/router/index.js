import Vue from 'vue'
import VueRouter from 'vue-router'
//import Home from '../views/Home.vue'
import store from "../store";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect:'/auth/login'
  },
  {
    path: "/auth",
    name: "login_auth",
    component:()=>import('../layout/LayoutLogin'),
    meta:{
      forVisitor:true
    },
    children:[
      {
        path:"login",
        name:"login",
        component:()=>import('../components/Auth/LoginComponent.vue')
      },
      {
        path:"register",
        name:"register",
        component:()=>import('../components/Auth/RegisterComponent')
      }
    ]
  },
  {
    path: "/app",
    name: "app",
    component:()=>import('../layout/LayoutMain'),
    meta:{
      forAuth: true
    },
    children:[
      {
        path:"home",
        name:"home",
        component:()=>import('../views/Home')
      },
    ]
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
router.beforeEach((to,from, next)=>{
  if(to.matched.some((record)=> record.meta.forAuth)){
    if (!store.getters.token){
      next({
        name:"login"
      });
    }
  }
  if (to.matched.some((record)=>record.meta.forVisitor)){
    if (store.getters.token){
      next("app/home");
    }
  }
  next();

});

export default router
