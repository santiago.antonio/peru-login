import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex)
const getDefaultState = () => {
  return {
    token: null,
  };
};
const store = new Vuex.Store({
  state: getDefaultState,
  getters:{
    token: state => {
      return state.token;
    },
  },
  mutations: {
    token: (state, payload) => {
      state.token = payload;
    },
  },
  actions: {
    token: ({ commit }, payload) => {
      commit("token", payload);
    },
  },
  modules: {
  },
  plugins: [createPersistedState()],
});
export default store;
